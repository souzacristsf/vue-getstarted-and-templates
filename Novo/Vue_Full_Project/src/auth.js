export default {
  setLogin: function (data) {
    localStorage.setItem('username', data.user.username)
    localStorage.setItem('fullname', data.user.fullname)
    localStorage.setItem('token', data.token)
  },

  getLogin: function () {
    return {
      username: localStorage.getItem('username'),
      fullname: localStorage.getItem('fullname'),
      token: localStorage.getItem('token')
    }
  },
  logout: function () {
    localStorage.removeItem('username')
    localStorage.removeItem('fullname')
    localStorage.removeItem('token')
  }
}
