export default {
  isLogged: (state) => state.token !== '',
  getAuthUsername: (state) => state.username,
  getAuthFullname: (state) => state.fullname,
  getAuthToken: (state) => state.token
}
