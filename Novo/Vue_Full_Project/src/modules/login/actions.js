import { HTTP } from '../../http'
import Auth from '../../auth'

export const doLogin = (context, payload) => {
    return HTTP.post('/', payload).then(
      (response) => {
        context.commit('SETLOGIN', response.data)

        Auth.setLogin(response.data)
      })
    }
export const doLogout = (context) => {
    let login = {
      username: '',
      email: '',
      token: '',
      fullname: ''
    }
    context.commit('SETLOGIN', login)
}

export const checkUserToken = ({ dispatch, state }) => {
  // If the token exists then all validation has already been done
  if (!isEmpty(state.token)) {
    return Promise.resolve(state.token)
  }
  /**
   * Token does not exist yet
   * - Recover it from localstorage
   * - Recover also the user, validating the token also
   */
  return localforage.getItem(userTokenStorageKey)
    .then((token) => {
      if (isEmpty(token)) {
        // Token is not saved in localstorage
        return Promise.reject('NO_TOKEN') // Reject promise
      }
      // Put the token in the vuex store
      return dispatch('setToken', token) // keep promise chain
    })
    // With the token in hand, retrieves the user's data, validating the token
    .then(() => dispatch('loadUser'))
}
