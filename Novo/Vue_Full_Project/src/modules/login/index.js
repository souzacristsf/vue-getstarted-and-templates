import actions from './actions'
import getters from './getters'

const state = {
  username: '',
  email: '',
  token: '',
  fullname: ''
}

const mutations = {
  SETLOGIN (state, data) {
    state.username = data.user.username
    state.email = data.user.email
    state.token = data.token
    state.fullname = data.user.fullname
  }
}

export default { state, mutations, actions, getters }
