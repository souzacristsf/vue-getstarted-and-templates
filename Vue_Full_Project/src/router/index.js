import Vue from 'vue'
import Router from 'vue-router'
import beforeEach from './beforeEach'
import { routes as app } from '../app'

// Containers
// import Full from '@/containers/Full'

// Views
// import Dashboard from '@/views/Dashboard'
// import Conexao from '@/views/Conexao'

// import Charts from '@/views/Charts'
// import Widgets from '@/views/Widgets'

// Views - Components
// import Buttons from '@/views/components/Buttons'
// import SocialButtons from '@/views/components/SocialButtons'
// import Cards from '@/views/components/Cards'
// import Forms from '@/views/components/Forms'
// import Modals from '@/views/components/Modals'
// import Switches from '@/views/components/Switches'
// import Tables from '@/views/components/Tables'

// Views - Icons
// import FontAwesome from '@/views/icons/FontAwesome'
// import SimpleLineIcons from '@/views/icons/SimpleLineIcons'

// Views - Pages
// import Page404 from '@/views/pages/Page404'
// import Page500 from '@/views/pages/Page500'
// import Login from '@/app/auth/components/forms/Login'
// import Register from '@/app/auth/components/forms/Register'

Vue.use(Router)

// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Operators/Spread_operator
const routes = [...app]

const router = new Router({
  routes,
  linkActiveClass: 'active',
  mode: 'history' // do not use /#/.
})

/**
* Before a route is resolved we check for
* the token if the route is marked as
* requireAuth.
*/
router.beforeEach(beforeEach)

export default router
// mode: 'hash', // Demo is living in GitHub.io, so required!
// linkActiveClass: 'open active',
// scrollBehavior: () => ({ y: 0 })
// routes: [
//   {
//     path: '/sign_in',
//     // redirect: '/login',
//     name: 'Signin',
//     component: Login,
//     meta: { requiresAuth: false }
//   },
//   {
//     path: '/register',
//     name: 'Register',
//     component: Register,
//     meta: { requiresAuth: false }
//   },
//   {
//     path: '/home',
//     // redirect: '/dashboard',
//     name: 'Home',
//     component: Full,
//     children: [
//       {
//         path: '/conexao',
//         name: 'Conexao',
//         component: Conexao,
//         meta: { requiresAuth: true }
//       },
//       {
//         path: '/dashboard',
//         name: 'Dashboard',
//         component: Dashboard,
//         meta: { requiresAuth: true }
//       }
//     ]
//     //   {
//     //     path: '/charts',
//     //     name: 'Charts',
//     //     component: Charts
//     //   },
//     //   {
//     //     path: '/widgets',
//     //     name: 'Widgets',
//     //     component: Widgets
//     //   },
//     //   {
//     //     path: '/components',
//     //     redirect: '/components/buttons',
//     //     name: 'Components',
//     //     component: {
//     //       render (c) { return c('router-view') }
//     //     },
//     //     children: [
//     //       {
//     //         path: '/buttons',
//     //         name: 'Buttons',
//     //         component: Buttons
//     //       },
//     //       {
//     //         path: '/social-buttons',
//     //         name: 'Social Buttons',
//     //         component: SocialButtons
//     //       },
//     //       {
//     //         path: 'cards',
//     //         name: 'Cards',
//     //         component: Cards
//     //       },
//     //       {
//     //         path: 'forms',
//     //         name: 'Forms',
//     //         component: Forms
//     //       },
//     //       {
//     //         path: 'modals',
//     //         name: 'Modals',
//     //         component: Modals
//     //       },
//     //       {
//     //         path: 'switches',
//     //         name: 'Switches',
//     //         component: Switches
//     //       },
//     //       {
//     //         path: 'tables',
//     //         name: 'Tables',
//     //         component: Tables
//     //       }
//     //     ]
//     //   },
//     //   {
//     //     path: '/icons',
//     //     redirect: '/icons/font-awesome',
//     //     name: 'Icons',
//     //     component: {
//     //       render (c) { return c('router-view') }
//     //     },
//     //     children: [
//     //       {
//     //         path: '/font-awesome',
//     //         name: 'Font Awesome',
//     //         component: FontAwesome
//     //       },
//     //       {
//     //         path: '/simple-line-icons',
//     //         name: 'Simple Line Icons',
//     //         component: SimpleLineIcons
//     //       }
//     //     ]
//     //   }
//     // ]
//   // },
//   // {
//   //   path: '/pages',
//   //   redirect: '/pages/404',
//   //   name: 'Pages',
//   //   component: {
//   //     render (c) { return c('router-view') }
//   //   },
//   //   children: [
//   //     {
//   //       path: '404',
//   //       name: 'Page404',
//   //       component: Page404
//   //     },
//   //     {
//   //       path: '500',
//   //       name: 'Page500',
//   //       component: Page500
//   //     },
//   //     {
//   //       path: '/register',
//   //       name: 'Register',
//   //       component: Register
//   //     }
//   //   ]
//   }
// ]
// })
