
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
const Full = r => require.ensure([], () => r(require('./Full')), 'full-bundle')

const meta = {
  requiresAuth: true,
}

export default [
  {
    path: '/home',
    name: 'Home',
    component: Full,
    meta
    // children: [
    //   {
    //     path: '/conexao',
    //     name: 'Conexao',
    //     component: Conexao,
    //     meta
    //   },
    //   {
    //     path: '/dashboard',
    //     name: 'Dashboard',
    //     component: Dashboard,
    //     meta
    //   }
    // ]
  }
]
