import { Signup, Signin } from './components'

export default [
  {
    path: '/sign_in',
    name: 'Signin',
    component: Login,
    meta: { requiresAuth: false }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { requiresAuth: false }
  }
]
