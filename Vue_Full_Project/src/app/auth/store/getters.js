export const isLogged = (state) => state.token !== ''

export const getAuthUsername = (state) => state.username

export const getAuthFullname = (state) => state.fullname

export const getAuthToken = (state) => state.token
