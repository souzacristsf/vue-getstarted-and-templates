import actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import state from './state'

const module = { state, mutations, actions, getters }

export default { module }
