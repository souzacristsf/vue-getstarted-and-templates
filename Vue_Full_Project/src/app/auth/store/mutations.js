import * as TYPES from './types'

export default {
  [TYPES.SET_TOKEN] (state, obj) {
    state.username = obj.user.username
    state.email = obj.user.email
    state.token = obj.token
    state.fullname = obj.user.fullname
  }
}
