import { HTTP } from '../../../http'
import Auth from '../../../auth'
import * as TYPES from './types'
import { isEmpty } from 'lodash'

export default {
  doLogin ({ commit }, payload) {
    return HTTP.post('/', payload).then(
      (response) => {
        commit(TYPES.SET_TOKEN, response.data)

        Auth.setLogin(response.data)
      })
  },

  doLogout ({ commit }) {
    let login = {
      username: '',
      email: '',
      token: '',
      fullname: ''
    }
    commit(TYPES.SET_TOKEN, login)
  },

  setToken ({ commit }, payload) {
    // prevent if payload is a object
    const token = (isEmpty(payload)) ? null : payload.token || payload

    // Commit the mutations
    commit(TYPES.SET_TOKEN, token)

    return Promise.resolve(token) // keep promise chain
  },

  checkUserToken ({ dispatch, state }){
    // If the token exists then all validation has already been done
    if (!isEmpty(state.user.token)) {
      return Promise.resolve(state.user.token)
    }

    /**
     * Token does not exist yet
     * - Recover it from localstorage
     * - Recover also the user, validating the token also
     */
    return Auth.getUser()
      .then(({token}) => {
        if (isEmpty(token)) {
          // Token is not saved in localstorage
          return Promise.reject('NO_TOKEN') // Reject promise
        }
        // Put the token in the vuex store
        return dispatch('setToken', token) // keep promise chain
      })
      // With the token in hand, retrieves the user's data, validating the token
      .then(() => 'Muito bom')
  },


}
