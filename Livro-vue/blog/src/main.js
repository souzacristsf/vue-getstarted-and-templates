// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable */
import Vue from 'vue'
import App from './App'
import router from './router'
import VueFire from 'vuefire'
import firebase from 'firebase'
import store from './store'

Vue.config.productionTip = false

Vue.use(VueFire)
  // Initialize Firebase
firebase.initializeApp({
  apiKey: 'AIzaSyCG9-DrIXvlT7fbuUi_r8Q998TEMyWuTjM',
  authDomain: 'vue2napratica-project.firebaseapp.com',
  databaseURL: 'https://vue2napratica-project.firebaseio.com',
  projectId: 'vue2napratica-project',
  storageBucket: '',
  messagingSenderId: '275031665981'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  data: {
    store
  }
})

require('../node_modules/bulma/css/bulma.css')
require('./bradcomp')
