/* eslint-disable */

(function() {
  var burger = document.querySelector('.navbar-burger')
  var menu = document.querySelector('.navbar-menu')
  burger.addEventListener('click', () => {
    burger.classList.toggle('is-active')
    menu.classList.toggle('is-active')
  })
})()
