// src/router.js
/* eslint-disable */
const store = {
  debug: true,
  state: {
    user: null
  },

setUser (user) {
  if (this.debug) console.log('store.setUser ', user)
  this.state.user = user
},

removeUser () {
  if (this.debug) console.log('store.removeUser ')
  this.state.user = null
},

isLogged () {
  if (this.debug) console.log('store.isLogged? ', this.state.user != null)
  return this.state.user != null
}

}

export default store
