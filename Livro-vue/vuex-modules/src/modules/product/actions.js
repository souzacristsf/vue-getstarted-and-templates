import { HTTP } from '../../http';

export default {
    loadProducts(context){
        HTTP.get("/products.json").then(
            (response) => {
                console.log(response)
                context.commit("SETPRODUCTS", response.data)
            },
            (error)=>{
                console.error(error.statusText)
        })
    }
}