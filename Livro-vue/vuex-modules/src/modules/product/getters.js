export default { 
    getProducts: state => {
        // console.log('state: ',state)
        return state.list;
    },

    hasProducts: state => {
        return state.list.length > 0
    }
}   