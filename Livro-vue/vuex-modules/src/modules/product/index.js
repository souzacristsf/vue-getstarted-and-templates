import actions from './actions'
import getters from './getters'

const state = {
    list : [],
    selected : {}
}

const mutations = {
    SETPRODUCTS(state, data) {
        state.list = data;
    }
}

export default { state, mutations, actions, getters }