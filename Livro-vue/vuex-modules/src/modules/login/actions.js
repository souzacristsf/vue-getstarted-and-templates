import { HTTP } from '../../http';

export default {
    doLogin(context){

        HTTP.get('/login.json').then(
            (response) => {
                context.commit("SETLOGIN", response.data)
            },
            (error)=>{
                console.error(error.statusText)
            }
        )

    },

    doLogout(context,){

      let login = {
          username : "",
          email : "",
          token : ""
      }

      context.commit("SETLOGIN", login)

    }

}