import axios from 'axios'

export const HTTP = axios.create({
  baseURL: '../json',
  timeout: 1000,
  headers: {'X-Custom-Header': 'foobar'}
})

// const HTTP = axios.create({
//   baseURL: '../login.json'
// });

// export default HTTP; 