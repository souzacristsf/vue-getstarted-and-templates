import actionslogin from './modules/login/actions'
import actionsProduct from './modules/product/actions'
import actionsSupplier from './modules/supplier/actions'
import actionsUser from './modules/user/actions'

export default {
    actionslogin,
    actionsProduct,
    actionsSupplier,
    actionsUser
}