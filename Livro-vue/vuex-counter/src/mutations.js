export default {

    INCREMENT(state, payload){

        if(payload){
            state.state.count = payload;        
        } else {
            state.state.count++;        
        }
    },

    DECREMENT(state){
        state.state.count--;
    },

    INCREMENTVALUE(state, value){
        state.state.count = parseInt(state.state.count) + parseInt(value)
    },

    SHOW_WAIT_MESSAGE(state){
        state.state.showWaitMessage = true;
    },

    HIDE_WAIT_MESSAGE(state){
        state.state.showWaitMessage = false;
    }
}