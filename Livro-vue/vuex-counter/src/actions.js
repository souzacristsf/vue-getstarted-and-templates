export default {
    changeIncrement(context, payload ){
        context.commit( 'SHOW_WAIT_MESSAGE' );
        return new Promise( ( resolve ) => {
            setTimeout( () => {
                context.commit( 'HIDE_WAIT_MESSAGE' );
                context.commit( 'INCREMENT', payload );
                 resolve();
            } , 2000 )
        })
    },

    changeDecrement(context, payload){
        context.commit('DECREMENT', payload)
    },

    incrementCounterWithValue(context, payload){
        let intValue = parseInt(payload);
        if (isNaN(intValue)) {
            throw "Impossível converter para número inteiro"
        } else {
            context.commit('INCREMENTVALUE', payload)
        }
    }

}