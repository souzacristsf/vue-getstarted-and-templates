export default {
  'SET_LIST'(state, obj){
    state.list = obj
  },
  'SET_FILTER'(state, obj){
    state.filter = obj
  }
}
