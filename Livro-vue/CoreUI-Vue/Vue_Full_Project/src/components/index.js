import Aside from './Aside.vue'
import Breadcrumb from './Breadcrumb.vue'
import Callout from './Callout.vue'
import Footer from './Footer.vue'
import Header from './Header.vue'
import Sidebar from './Sidebar.vue'
import Switch from './Switch.vue'
import Conection from '../app/conection/Main.vue'
import Signin from '../app/auth/Signin.vue'
import Signup from '../app/auth/Signup.vue'
import Dashboard from '../app/dashboard/Main.vue'

export {
  Aside,
  Breadcrumb,
  Callout,
  Footer,
  Header,
  Sidebar,
  Switch,
  Conection,
  Signin,
  Signup,
  Dashboard
}
