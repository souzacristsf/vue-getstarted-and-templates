 export default {
    localidade: state => {
      return `Este usuario mora em ${ state.user.city } - ${ state.user.state }`
    }
 }
